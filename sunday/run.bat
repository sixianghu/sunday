echo off
D:
SET JAVAFANS_HOME=%cd%
SET TOMCAT2_HOME=D:\soft\tomcat\redgift
cd %JAVAFANS_HOME%
SET MAVEN_OPTS=-Xms256m -Xmx512m -XX:ReservedCodeCacheSize=64m -XX:MaxPermSize=128m

:mvn_command
ECHO 0-生成三和进销存管理框架的eclipse项目工程文件
ECHO 1-编译三和进销存管理代码,不执行测试
ECHO 2-编译三和进销存管理管理后台代码,启动tomcat
ECHO 3-备份数据库

set /p isopt=【选择命令】
if /i "%isopt%"=="0" goto mvn_eclipse
if /i "%isopt%"=="1" goto mvn_compile
if /i "%isopt%"=="2" goto mvn_tocmat
if /i "%isopt%"=="3" goto backup_mysql

echo "无效选项，请选择(0-3)"
goto mvn_command

:mvn_eclipse
	echo [INFO]  开始生成平台相关eclipse文件
	cd %JAVAFANS_HOME%
	call mvn clean eclipse:eclipse
	goto mvn_end

:mvn_compile
	echo [INFO]  开始编译三和进销存管理代码
	cd %JAVAFANS_HOME%
	call mvn clean install -Dmaven.test.skip=true
	goto mvn_end

:mvn_tocmat
	echo [INFO]  开始编译打包管理后台 并启动web工程
	cd %JAVAFANS_HOME%
	call mvn clean  install -Dmaven.test.skip=true -Pcompany
	echo [INFO]  删除历史日志文件
	cd %LOG_HOME%
	del /q /s D:\logs\*.*
	echo [INFO]  启动tomcat
	cd %TOMCAT2_HOME%\bin
	call startup.bat
	goto mvn_end
	
:backup_mysql
	echo [INFO] 开始备份数据库redgift
	cd %JAVAFANS_HOME%
	call mysqldump -uroot -proot redgift > redgift.sql
	goto mvn_end

:mvn_end

cd %JAVAFANS_HOME%