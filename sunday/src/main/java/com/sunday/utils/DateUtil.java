/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author donghongli
 * @version $Id: DateUtil.java, v 0.1 2016年1月6日 上午10:39:00 donghongli Exp $
 */
public class DateUtil {

    public static final String DEFAULT_FORMT       = "yyyy-MM-dd hh:mm:ss";
    public static final String DATE_SIMPLE_FORMAT  = "yyyyMMdd";
    public static final String MONTH_SIMPLE_FORMAT = "yyyyMM";

    /**
     * 按照默认格式格式化日期
     * 
     * @param date
     * @return
     */
    public static String defaultFormat(Date date) {
        return format(date, DEFAULT_FORMT);
    }

    /**
     * 格式化日期
     * 
     * @param date
     * @param pattern
     * @return
     */
    public static String format(Date date, String pattern) {
        if (date == null || StringUtils.isBlank(pattern)) {
            return StringUtils.EMPTY;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);

    }

    /**
     * 获取当前时间---日
     * 
     * @return
     */
    public static int getCurrentDay() {
        return getDay(new Date());
    }

    /**
     * 获取指定时间---日
     * 
     * @param date
     * @return
     */
    public static int getDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

}
