/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.utils;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.sunday.bo.AreaBO;
import com.sunday.pojo.AreaDO;

/**
 * 地区工具类
 * @author donghongli
 * @version $Id: AreaUtil.java, v 0.1 2016年1月27日 下午5:03:32 donghongli Exp $
 */
public class AreaUtil {

    private static AreaBO areaBO = SpringContextHolder.getBean("areaBO");

    /**
     * 根据id获取名称
     * 
     * @param areaId
     * @return
     */
    public static String getNameById(Integer areaId) {
        if (areaId == null) {
            return StringUtils.EMPTY;
        }
        AreaDO area = areaBO.find(areaId);
        return (area != null) ? area.getName() : StringUtils.EMPTY;
    }

    /**
     * 查询上级地区
     * 
     * @param areaId
     * @return
     */
    public static AreaDO getparentName(Integer areaId) {
        if (areaId == null) {
            return null;
        }
        AreaDO area = areaBO.find(areaId);
        if (area == null) {
            return null;
        }
        if (area.getParentId() == 0) {
            return area;
        }
        return areaBO.find(area.getParentId());
    }

    /**
     * 查询下级地区列表
     * 
     * @param parentId
     * @return
     */
    public static List<AreaDO> getListByParentId(Integer parentId) {
        return areaBO.queryByParentId(parentId);
    }

}
