package com.sunday.utils;

public class GoodsVO {
    private Integer goodsId;
    private String  goodsName;
    private Double  shopPrice;

    private Integer catId;
    //    private Date    addTime;

    private String  createUserName;

    /**
     * @return the catId
     */
    public Integer getCatId() {
        return catId;
    }

    /**
     * @param catId the catId to set
     */
    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    /**
     * @return the createUserName
     */
    public String getCreateUserName() {
        return createUserName;
    }

    /**
     * @param createUserName the createUserName to set
     */
    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    //    /**
    //     * Getter method for property <tt>addTime</tt>.
    //     * 
    //     * @return property value of addTime
    //     */
    //    public Date getAddTime() {
    //        return addTime;
    //    }
    //
    //    /**
    //     * Setter method for property <tt>addTime</tt>.
    //     * 
    //     * @param addTime value to be assigned to property addTime
    //     */
    //    public void setAddTime(Date addTime) {
    //        this.addTime = addTime;
    //    }

    /**
     * Getter method for property <tt>goodsId</tt>.
     * 
     * @return property value of goodsId
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * Setter method for property <tt>goodsId</tt>.
     * 
     * @param goodsId value to be assigned to property goodsId
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * Getter method for property <tt>goodsName</tt>.
     * 
     * @return property value of goodsName
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * Setter method for property <tt>goodsName</tt>.
     * 
     * @param goodsName value to be assigned to property goodsName
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * Getter method for property <tt>shopPrice</tt>.
     * 
     * @return property value of shopPrice
     */
    public Double getShopPrice() {
        return shopPrice;
    }

    /**
     * Setter method for property <tt>shopPrice</tt>.
     * 
     * @param shopPrice value to be assigned to property shopPrice
     */
    public void setShopPrice(Double shopPrice) {
        this.shopPrice = shopPrice;
    }

}