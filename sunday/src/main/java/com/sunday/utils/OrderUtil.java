/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.utils;

import java.util.ArrayList;
import java.util.List;

import com.sunday.enums.OrderEnum;
import com.sunday.enums.OrderEnum.OrderTypeEnum;

/**
 * 订单工具类
 * @author donghongli
 * @version $Id: OrderUtil.java, v 0.1 2016年2月29日 上午11:35:15 donghongli Exp $
 */
public class OrderUtil {

    public static List<OrderTypeEnum> getOrderTypes() {
        List<OrderTypeEnum> orderTypes = new ArrayList<OrderTypeEnum>();
        for (OrderTypeEnum orderTypeEnum : OrderEnum.OrderTypeEnum.values()) {
            orderTypes.add(orderTypeEnum);
        }
        return orderTypes;
    }
}
