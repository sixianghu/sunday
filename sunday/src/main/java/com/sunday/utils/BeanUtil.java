/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author donghongli
 * @version $Id: BeanUtil.java, v 0.1 2016年3月18日 上午10:14:31 donghongli Exp $
 */
public class BeanUtil {

    /**
     * 获取对象属性名称列表
     * @param objects 
     * @return
     */
    public static List<String> getPropertyNames(Object obj) {
        Field[] fields = obj.getClass().getDeclaredFields();
        List<String> fieldNames = new ArrayList<String>();
        for (Field field : fields) {
            fieldNames.add(field.getName());
        }
        return fieldNames;
    }
}
