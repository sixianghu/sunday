/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.utils;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.sunday.bo.GoodsCateGoryBO;
import com.sunday.pojo.GoodsCateGoryDO;

/**
 * 商品工具类
 * @author donghongli
 * @version $Id: GoodsUtil.java, v 0.1 2016年2月26日 上午11:36:08 donghongli Exp $
 */
public class GoodsUtil {

    private static GoodsCateGoryBO goodsCateGoryBO = SpringContextHolder.getBean("goodsCateGoryBO");

    public static String getCatName(Integer catId) {
        GoodsCateGoryDO cat = goodsCateGoryBO.getById(catId);
        return cat == null ? StringUtils.EMPTY : cat.getName();
    }

    public static List<GoodsCateGoryDO> getCatList() {
        return goodsCateGoryBO.list();
    }

}
