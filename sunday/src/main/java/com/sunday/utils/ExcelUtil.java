//package com.sunday.utils;
//
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.lang.reflect.InvocationTargetException;
//import java.util.Arrays;
//import java.util.List;
//
//import jxl.Cell;
//import jxl.Sheet;
//import jxl.Workbook;
//import jxl.format.Border;
//import jxl.format.BorderLineStyle;
//import jxl.format.Colour;
//import jxl.read.biff.BiffException;
//import jxl.write.Label;
//import jxl.write.WritableCellFormat;
//import jxl.write.WritableFont;
//import jxl.write.WritableSheet;
//import jxl.write.WritableWorkbook;
//import jxl.write.WriteException;
//
//import org.apache.commons.beanutils.BeanUtils;
//import org.apache.commons.collections.CollectionUtils;
//import org.apache.commons.lang.ArrayUtils;
//
//public class ExcelUtil {
//
//    public static String   filePath = "d:\\testJXL.xls";
//    public static String[] titles   = { "编号", "产品名称", "产品价格", "产品数量", "生产日期", "产地", "是否出口" };
//
//    public static void main(String[] args) throws InstantiationException, IllegalAccessException,
//                                          InvocationTargetException, NoSuchMethodException,
//                                          BiffException, FileNotFoundException, IOException {
//        //        String sql = "select goods_id,shop_price,goods_name from gzseed_vancelle_goods.gss_goods limit 10";
//        //        List<GoodsVO> goodsList = JDBCUtil.excute(sql, GoodsVO.class);
//        //
//        //        writeExcel(filePath, goodsList, null);
//        //        readExcel();
//
//    }
//
//    /**
//     * 读取excel
//     * @throws IOException
//     * @throws BiffException
//     * @throws FileNotFoundException
//     */
//    private static void readExcel() throws IOException, BiffException, FileNotFoundException {
//        Workbook book = Workbook.getWorkbook(new FileInputStream(filePath));
//        for (Sheet sheet : book.getSheets()) {
//            System.out.println(sheet.getName());
//            for (int i = 0; i < sheet.getRows(); i++) {
//                Cell[] cells = sheet.getRow(i);
//                for (Cell cell : cells) {
//                    System.out.print(cell.getContents() + "   ");
//                }
//                System.out.println();
//            }
//        }
//    }
//
//    /**
//     * 生成excel文件,字段顺序为POJO中属性排列顺序
//     * @param filePath 文件路径
//     * @param objects 要导出的文件列表
//     * @param labels 标签名
//     */
//    public static <T> void writeExcel(String filePath, List<T> objects, String[] labels) {
//        if (CollectionUtils.isEmpty(objects)) {
//            return;
//        }
//        List<String> titles = ArrayUtils.isNotEmpty(labels) ? Arrays.asList(labels) : BeanUtil
//            .getPropertyNames(objects.get(0));
//        try {
//            // 获得开始时间   
//            long start = System.currentTimeMillis();
//
//            // 创建Excel工作薄   
//            WritableWorkbook wwb;
//            // 新建立一个jxl文件,即在d盘下生成testJXL.xls   
//            OutputStream os = new FileOutputStream(filePath);
//            wwb = Workbook.createWorkbook(os);
//            // 添加第一个工作表并设置第一个Sheet的名字   
//            WritableSheet sheet = wwb.createSheet("sheet1", 0);
//            Label label;
//            for (int i = 0; i < titles.size(); i++) {
//                label = new Label(i, 0, titles.get(i));
//                sheet.addCell(label);
//            }
//            for (int i = 0; i < objects.size(); i++) {
//                for (int j = 0; j < titles.size(); j++) {
//                    String value = BeanUtils.getSimpleProperty(objects.get(i), titles.get(j));
//                    label = new Label(j, i + 1, value);
//                    sheet.addCell(label);
//                }
//            }
//
//            // 写入数据   
//            wwb.write();
//            // 关闭文件   
//            wwb.close();
//            long end = System.currentTimeMillis();
//            System.out.println("----完成该操作共用的时间是:" + (end - start) / 1000);
//        } catch (Exception e) {
//            System.out.println("---出现异常---");
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * excel样式
//     * 
//     * @return
//     */
//    public static WritableCellFormat getHeader() {
//        WritableFont font = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);//定义字体
//        try {
//            font.setColour(Colour.BLUE);//蓝色字体
//        } catch (WriteException e1) {
//            // TODO 自动生成 catch 块
//            e1.printStackTrace();
//        }
//        WritableCellFormat format = new WritableCellFormat(font);
//        try {
//            format.setAlignment(jxl.format.Alignment.CENTRE);//左右居中
//            format.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);//上下居中
//            format.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);//黑色边框
//            format.setBackground(Colour.YELLOW);//黄色背景
//        } catch (WriteException e) {
//            // TODO 自动生成 catch 块
//            e.printStackTrace();
//        }
//        return format;
//    }
//}
