/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sunday.bo.OrderItemBO;
import com.sunday.bo.StaGoodsOrderBO;
import com.sunday.bo.StaUserOrderBO;
import com.sunday.bo.TodayOrderInfoBO;

/**
 * 统计   商品每天销售金额
 * @author donghongli
 * @version $Id: DayTimeOrderTask.java, v 0.1 2016年1月6日 下午3:37:45 donghongli Exp $
 */
@Component("staGoodsOrderTask")
public class StaGoodsOrderTask {

    @Autowired
    private OrderItemBO      orderItemBO;
    @Autowired
    private StaGoodsOrderBO  staGoodsOrderBO;
    @Autowired
    private StaUserOrderBO   staUserOrderBO;
    @Autowired
    private TodayOrderInfoBO todayOrderInfoBO;

    //每天晚上23点30的时候，统计商品当天销售情况
    @Scheduled(cron = "0 30 23 * * ?")
    public void goodsOrderTask() {
        todayOrderInfoBO.importTodayOrderInfo();
        staGoodsOrderBO.importTodayGoodsSaleInfo();
    }

    //    //每月最后一天23点30的时候，统计用户当月进货情况
    //    @Scheduled(cron = "0 30 23 * * ?")
    //    //        TODO todayOrderInfo表存储的是当天订单信息,不能用来统计用户当月信息,需要考虑是否延长today表存储的订单日期,或者另外建一张表.
    //    public void userOrderTask() {
    //        System.out.println("执行统计");
    //        Date now = new Date();
    //        String currentMonth = DateUtil.format(now, DateUtil.MONTH_SIMPLE_FORMAT);
    //        List<StaUserOrderDO> userOrderStas = orderItemBO.staUserByMonth(currentMonth);
    //        System.out.println("共统计到" + userOrderStas.size() + "条数据.");
    //        for (StaUserOrderDO staUserOrderDO : userOrderStas) {
    //            staUserOrderDO.setCreated(now);
    //            staUserOrderDO.setModify(now);
    //            staUserOrderBO.save(staUserOrderDO);
    //        }
    //    }
}
