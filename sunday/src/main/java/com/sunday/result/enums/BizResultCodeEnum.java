/**
 * 
 */
package com.sunday.result.enums;

/**
 * 操作返回码枚举
 * @author donghongli
 *
 */
public enum BizResultCodeEnum {

    /* 操作成功 */
    SUCCESS("SUCCESS", 1000, "操作成功"),

    /* 系统错误 */
    SYSTEM_FAILURE("SYSTEM_FAILURE", 1001, "系统错误，稍后再试"),

    /* 新增的参数已经存在(唯一性约束) */
    DUPLICATE_KEY("DUPLICATE_KEY", 1003, "新增的参数已经存在(唯一性约束)"),

    /* 参数不正确 */
    ILLEGAL_ARGUMENT("ILLEGAL_ARGUMENT", 1004, "参数不正确"),

    /* 重复提交错误 */
    RESUBMIT_ERROR("RESUBMIT_ERROR", 1010, "重复提交错误"),

    /* 数据不存在 */
    NOT_EXISTED("NOT_EXISTED", 1011, "数据不存在"), ;

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 数值型错误码
     */
    private final int    codeNumber;

    /**
     * 枚举信息
     */
    private final String desc;

    /**
     * 构造函数
     *
     * @param code        枚举值
     * @param codeNumber  数值型错误码
     * @param desc 枚举信息
     */
    BizResultCodeEnum(String code, int codeNumber, String desc) {
        this.code = code;
        this.codeNumber = codeNumber;
        this.desc = desc;
    }

    /**
     * 根据code获取枚举
     *
     * @param code
     * @return
     */
    public static BizResultCodeEnum getByCode(String code) {
        for (BizResultCodeEnum item : values()) {
            if (code.equals(item.getCode())) {
                return item;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    public int getCodeNumber() {
        return codeNumber;
    }
}
