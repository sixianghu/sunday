/**
 * 
 */
package com.sunday.result;

import com.sunday.base.Page;
import com.sunday.result.enums.BizResultCodeEnum;

/**
 * 返回结果类
 * 
 * @author donghongli
 *
 */
public class BaseResult<T> extends Page {

	private boolean isOk;

	private String code;

	private String desc;

	private T t;

	public BaseResult(T objT) {
		this.t = objT;
		this.isOk = true;
	}

	public BaseResult(T objT, int currentPage, int pageSize, int totalRows) {
		this.t = objT;
		setCurrentPage(currentPage);
		setRows(pageSize);
		setTotalRows(totalRows);
		this.isOk = true;
	}

	public BaseResult(T objT, Page page, int totalRows) {
		this.t = objT;
		this.isOk = true;

		setCurrentPage(page.getCurrentPage());
		setOrderBy(page.getOrderBy());
		setOrderType(page.getOrderType());
		setRows(page.getRows());
		setTotalRows(totalRows);
		setUrl(page.getUrl());

	}

	public BaseResult(BizResultCodeEnum resultCode) {
		this.code = resultCode.getCode();
		this.desc = resultCode.getDesc();
		this.isOk = false;
	}

	/**
	 * @return the isOk
	 */
	public boolean isOk() {
		return isOk;
	}

	/**
	 * @param isOk
	 *            the isOk to set
	 */
	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the t
	 */
	public T getT() {
		return t;
	}

	/**
	 * @param t
	 *            the t to set
	 */
	public void setT(T t) {
		this.t = t;
	}

	/**
	 * 
	 * @return BaseResult<Boolean>
	 * @exception
	 */
	public static <T> BaseResult<T> success(T t) {
		BaseResult<T> result = new BaseResult<T>(BizResultCodeEnum.SUCCESS);
		result.setOk(true);
		result.setT(t);
		return result;
	}

}
