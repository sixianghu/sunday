/**
 * 万色城基础服务系统
 * 2015年11月12日-上午10:22:25
 * 2015杭州万色城电子商务有限公司-版权所有
 * 
 */
package com.sunday.result;

import com.sunday.result.enums.BizResultCodeEnum;

/**
 * 业务异常
 * 
 * @author donghongli 2015年11月12日 上午10:22:25
 * @version 1.1.0
 * 
 */
public class BizException extends RuntimeException {

    /** serialVersionUID */
    private static final long serialVersionUID = -5353102099168631668L;

    /* 结果码 */
    private String            code             = "UNKNOWN";

    /** 数值型结果码 */
    private int               codeNumber       = 101;

    /** 异常信息 */
    private String            message          = "未知异常";

    /**
     * 构造方法
     */
    public BizException() {
    }

    /**
     * 构造方法
     */
    public BizException(String message) {
        this.message = message;
        this.code = BizResultCodeEnum.SYSTEM_FAILURE.getCode();
    }

    /**
     * 构造方法
     * 
     * @param resultCode
     *            错误码
     */
    public BizException(BizResultCodeEnum resultCode) {
        this.code = resultCode.getCode();
        this.codeNumber = resultCode.getCodeNumber();
        this.message = resultCode.getDesc();
    }

    /**
     * 构造方法
     * 
     * @param resultCode
     *            错误码
     * @param message
     *            补充错误信息
     */
    public BizException(BizResultCodeEnum resultCode, String message) {
        this.code = resultCode.getCode();
        this.codeNumber = resultCode.getCodeNumber();
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public int getCodeNumber() {
        return codeNumber;
    }

}
