/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.constant;

/**
 * 正则校验表达式常量
 * @author donghongli
 * @version $Id: RegularConstant.java, v 0.1 2016年1月26日 上午10:30:23 donghongli Exp $
 */
public class RegularConstant {

    /***
     * 手机号校验,11位数字,且是以13\15\17\18开头
     */
    public static final String MOBILE = "^1[3578][0-9]{9}$";

}
