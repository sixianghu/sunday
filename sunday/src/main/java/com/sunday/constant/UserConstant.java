/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.constant;

/**
 * 用户相关常量
 * @author donghongli
 * @version $Id: UserConstant.java, v 0.1 2016年1月26日 下午6:15:43 donghongli Exp $
 */
public class UserConstant {

    /**
     * 用户默认密码
     */
    public static final String DEFAULT_PWD = "password";

}
