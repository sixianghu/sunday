/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sunday.bo.OrderBO;
import com.sunday.bo.StaGoodsOrderBO;
import com.sunday.bo.StaUserOrderBO;
import com.sunday.form.OrderQueryForm;
import com.sunday.pojo.OrderDO;
import com.sunday.pojo.StaGoodsOrderDO;
import com.sunday.pojo.StaUserOrderDO;

/**
 * 用户销售情况统计信息
 * @author donghongli
 * @version $Id: StaUserController.java, v 0.1 2016年1月12日 下午4:40:32 donghongli Exp $
 */
@Controller
@RequestMapping("/sta")
public class StaController {

    @Autowired
    private StaUserOrderBO  staUserOrderBO;
    @Autowired
    private OrderBO         orderBO;
    @Autowired
    private StaGoodsOrderBO staGoodsOrderBO;

    /**
     * 用户销售详情
     * 
     * @param map
     * @param userId
     * @return
     */
    @RequestMapping("/user/{userId}/sale/info")
    public String staUserOrderInfo(@PathVariable("userId") long userId, ModelMap map) {
        OrderQueryForm form = new OrderQueryForm();
        form.setBuyerId(userId);
        form.setRows(15);
        List<OrderDO> orders = orderBO.queryList(form);
        List<StaUserOrderDO> userOrderDOs = new ArrayList<StaUserOrderDO>();
        for (OrderDO order : orders) {
            StaUserOrderDO userOrder = new StaUserOrderDO();
            userOrder.setAmount(order.getAmount());
            userOrder.setMonth("1989/12/15");
            userOrderDOs.add(userOrder);
        }
        //        List<StaUserOrderDO> userOrderDOs = staUserOrderBO.lastTimeMonths(7, userId);
        map.put("userOrders", userOrderDOs);
        return "/sta/user_sale_info";
    }

    /**
     * 商品销售详情
     * 
     * @return
     */
    @RequestMapping("/goods/{goodsId}/sale/info")
    public String staGoodsInfo(@PathVariable("goodsId") long goodsId, ModelMap map) {
        List<StaGoodsOrderDO> goodsSaleInfos = staGoodsOrderBO.list(goodsId);
        map.put("goodsSales", goodsSaleInfos);

        return "/sta/goods_sale_info";
    }
}
