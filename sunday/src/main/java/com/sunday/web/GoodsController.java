/**
 * 
 */
package com.sunday.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sunday.bo.GoodsBO;
import com.sunday.bo.GoodsCateGoryBO;
import com.sunday.form.GoodsQueryForm;
import com.sunday.pojo.GoodsCateGoryDO;
import com.sunday.pojo.GoodsDO;
import com.sunday.result.BaseResult;

/**
 * @author donghongli
 *
 */
@Controller
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsBO         goodsBO;
    @Autowired
    private GoodsCateGoryBO goodsCateGoryBO;

    /**
     * 商品列表
     * 
     * @param form
     * @param map
     * @return String
     * @exception
     */
    @RequestMapping("/list")
    public String list(GoodsQueryForm form, ModelMap map) {
        BaseResult<List<GoodsDO>> result = goodsBO.queryList(form);
        map.put("result", result);
        map.put("form", form);
        return "/goods/list";
    }

    @RequestMapping("/cat/list")
    public String catList(ModelMap map) {
        List<GoodsCateGoryDO> goodsCats = goodsCateGoryBO.list();
        map.put("list", goodsCats);
        return "goods/cat/list";
    }

    /**
     * 跳转到商品修改页面
     * 
     * @return String
     * @exception
     */
    @RequestMapping("/modify")
    public String modify(Long goodsId, ModelMap map) {
        if (goodsId != null) {
            GoodsDO goodsDO = goodsBO.getById(goodsId);
            map.put("goods", goodsDO);
        }
        return "/goods/modify";
    }

    /**
     * 保存或修改
     * 
     * @param goodsDO
     * @return BaseResult<Boolean>
     * @exception
     */
    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(GoodsDO goodsDO) {
        goodsBO.saveOrUpdate(goodsDO);
        return "redirect:/goods/list";
    }
}
