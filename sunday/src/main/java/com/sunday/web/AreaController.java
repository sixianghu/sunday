/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sunday.bo.AreaBO;
import com.sunday.pojo.AreaDO;

/**
 * 地区Controller
 * @author donghongli
 * @version $Id: AreaController.java, v 0.1 2016年1月27日 上午10:59:54 donghongli Exp $
 */
@Controller
@RequestMapping("/area")
public class AreaController {

    @Autowired
    private AreaBO areaBO;

    @RequestMapping("/list")
    public String listByParent(Integer parentId, ModelMap map) {
        List<AreaDO> areas = areaBO.queryByParentId(parentId);
        map.put("areas", areas);
        return "/area/list";
    }

    @RequestMapping("/find")
    public String find(Integer areaId, ModelMap map) {
        if (areaId != null) {
            AreaDO areaDO = areaBO.find(areaId);
            map.put("area", areaDO);
        }
        return "/area/modify";
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(AreaDO areaDO) {
        areaBO.saveOrUpdate(areaDO);
        return "redirect:/area/list.htm?parentId=0";
    }

    @RequestMapping("/delete")
    public String delete(Integer areaId) {
        areaBO.delete(areaId);
        return "redirect:/area/list.htm?parentId=0";
    }

}
