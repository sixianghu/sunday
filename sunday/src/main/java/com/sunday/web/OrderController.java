/**
 * 万色城基础服务系统
 * 2015年11月13日-下午4:23:58
 * 2015杭州万色城电子商务有限公司-版权所有
 * 
 */
package com.sunday.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sunday.bo.OrderBO;
import com.sunday.form.OrderQueryForm;
import com.sunday.pojo.OrderDO;
import com.sunday.result.BaseResult;

/**
 * 订单Controller
 * 
 * @author donghongli 2015年11月13日 下午4:23:58
 * @version 1.1.0
 * 
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderBO orderBO;

    @RequestMapping("/list")
    public String list(OrderQueryForm form, ModelMap map) {
        BaseResult<List<OrderDO>> result = orderBO.pageQuery(form);
        map.put("result", result);
        map.put("form", form);
        return "/order/list";
    }

    @RequestMapping("/find")
    public String find(Long orderId, ModelMap map) {
        OrderDO orderDO = orderBO.find(orderId);
        map.put("orderDO", orderDO);
        return "/order/find";
    }
}
