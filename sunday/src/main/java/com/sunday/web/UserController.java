/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sunday.bo.UserBO;
import com.sunday.form.UserQueryForm;
import com.sunday.pojo.UserDO;
import com.sunday.result.BaseResult;

/**
 * 用户Controller
 * @author donghongli
 * @version $Id: UserController.java, v 0.1 2016年1月6日 下午1:46:02 donghongli Exp $
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserBO userBO;

    @RequestMapping("/list")
    public String list(UserQueryForm form, ModelMap map) {
        BaseResult<List<UserDO>> result = userBO.queryList(form);
        map.put("result", result);
        map.put("form", form);
        return "/user/list";
    }

    @RequestMapping("/modify")
    public String modify(Long userId, ModelMap map) {
        if (userId != null) {
            UserDO user = userBO.find(userId);
            map.put("user", user);
        }
        return "/user/modify";
    }

    @RequestMapping("/saveOrModify")
    public String saveOrModify(UserDO userDO) {
        userBO.saveOrUpdate(userDO);
        return "redirect:/user/list";
    }

    @RequestMapping("/delete")
    public String delete(Long userId, ModelMap map) {
        userBO.delete(userId);
        return "redirect:/user/list";
    }
}
