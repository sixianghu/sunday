/**
 * 万色城基础服务系统
 * 2015年11月13日-下午3:17:26
 * 2015杭州万色城电子商务有限公司-版权所有
 * 
 */
package com.sunday.form;

import com.sunday.base.Page;

/**
 * 订单列表查询Form
 * 
 * @author donghongli 2015年11月13日 下午3:17:26
 * @version 1.1.0
 * 
 */
public class OrderQueryForm extends Page {

    private Long    id;

    private Long    buyerId;

    private String  buyerName;

    private Long    cashierId;

    private String  cashierName;

    private Integer type;

    private Integer status;

    private Integer payStatus;

    private String  startDay;

    private String  endDay;

    /**
     * 获取id的值
     *
     * @return the id
     *
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置 id的值
     * 
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取buyerId的值
     *
     * @return the buyerId
     *
     */
    public Long getBuyerId() {
        return buyerId;
    }

    /**
     * 设置 buyerId的值
     * 
     * @param buyerId
     *            the buyerId to set
     */
    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    /**
     * 获取buyerName的值
     *
     * @return the buyerName
     *
     */
    public String getBuyerName() {
        return buyerName;
    }

    /**
     * 设置 buyerName的值
     * 
     * @param buyerName
     *            the buyerName to set
     */
    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    /**
     * 获取cashierId的值
     *
     * @return the cashierId
     *
     */
    public Long getCashierId() {
        return cashierId;
    }

    /**
     * 设置 cashierId的值
     * 
     * @param cashierId
     *            the cashierId to set
     */
    public void setCashierId(Long cashierId) {
        this.cashierId = cashierId;
    }

    /**
     * 获取cashierName的值
     *
     * @return the cashierName
     *
     */
    public String getCashierName() {
        return cashierName;
    }

    /**
     * 设置 cashierName的值
     * 
     * @param cashierName
     *            the cashierName to set
     */
    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }

    /**
     * 获取type的值
     *
     * @return the type
     *
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置 type的值
     * 
     * @param type
     *            the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取status的值
     *
     * @return the status
     *
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置 status的值
     * 
     * @param status
     *            the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取payStatus的值
     *
     * @return the payStatus
     *
     */
    public Integer getPayStatus() {
        return payStatus;
    }

    /**
     * 设置 payStatus的值
     * 
     * @param payStatus
     *            the payStatus to set
     */
    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public String getStartDay() {
        return startDay;
    }

    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }

    public String getEndDay() {
        return endDay;
    }

    public void setEndDay(String endDay) {
        this.endDay = endDay;
    }

}
