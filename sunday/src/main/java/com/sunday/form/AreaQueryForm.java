/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.form;

/**
 * 地区查询Form
 * @author donghongli
 * @version $Id: AreaQueryForm.java, v 0.1 2016年1月27日 上午11:03:25 donghongli Exp $
 */
public class AreaQueryForm {

    private Long   parentId;

    private Long   areaId;

    private String areaName;

    /**
     * Getter method for property <tt>parentId</tt>.
     * 
     * @return property value of parentId
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * Setter method for property <tt>parentId</tt>.
     * 
     * @param parentId value to be assigned to property parentId
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * Getter method for property <tt>areaId</tt>.
     * 
     * @return property value of areaId
     */
    public Long getAreaId() {
        return areaId;
    }

    /**
     * Setter method for property <tt>areaId</tt>.
     * 
     * @param areaId value to be assigned to property areaId
     */
    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    /**
     * Getter method for property <tt>areaName</tt>.
     * 
     * @return property value of areaName
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * Setter method for property <tt>areaName</tt>.
     * 
     * @param areaName value to be assigned to property areaName
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

}
