/**
 * 万色城基础服务系统
 * 2015年11月13日-下午3:41:16
 * 2015杭州万色城电子商务有限公司-版权所有
 * 
 */
package com.sunday.form;

/**
 * 订单项列表查询Form
 * 
 * @author donghongli 2015年11月13日 下午3:41:16
 * @version 1.1.0
 * 
 */
public class OrderItemQueryForm {

	private Long orderId;

	private Long goodsId;
	
	private Integer goodsType;

	private Byte isValid;

	/**
	 * 获取isValid的值
	 *
	 * @return the isValid
	 *
	 */
	public Byte getIsValid() {
		return isValid;
	}

	/**
	 * 设置 isValid的值
	 * 
	 * @param isValid
	 *            the isValid to set
	 */
	public void setIsValid(Byte isValid) {
		this.isValid = isValid;
	}

	/**
	 * 获取orderId的值
	 *
	 * @return the orderId
	 *
	 */
	public Long getOrderId() {
		return orderId;
	}

	/**
	 * 设置 orderId的值
	 * 
	 * @param orderId
	 *            the orderId to set
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	/**
	 * 获取goodsId的值
	 *
	 * @return the goodsId
	 *
	 */
	public Long getGoodsId() {
		return goodsId;
	}

	/**
	 * 设置 goodsId的值
	 * 
	 * @param goodsId
	 *            the goodsId to set
	 */
	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	/**
	 * 获取goodsType的值
	 *
	 * @return the goodsType
	 *
	 */
	public Integer getGoodsType() {
		return goodsType;
	}

	/**
	 * 设置 goodsType的值
	 * 
	 * @param goodsType
	 *            the goodsType to set
	 */
	public void setGoodsType(Integer goodsType) {
		this.goodsType = goodsType;
	}

}
