/**
 * 
 */
package com.sunday.form;

import com.sunday.base.Page;

/**
 * 商品列表查询Form
 * @author donghongli
 *
 */
public class GoodsQueryForm extends Page {

	private String name;

	private Integer size;

	private Integer type;

	private Integer isOnSale;

	private Double minPrice;

	private Double maxPrice;
	
	private Integer minNum;

	private Integer maxNum;

	/**
	 * 获取name的值
	 *
	 * @return the name
	 *
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置 name的值
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取size的值
	 *
	 * @return the size
	 *
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * 设置 size的值
	 * 
	 * @param size
	 *            the size to set
	 */
	public void setSize(Integer size) {
		this.size = size;
	}

	/**
	 * 获取type的值
	 *
	 * @return the type
	 *
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * 设置 type的值
	 * 
	 * @param type
	 *            the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * 获取isOnSale的值
	 *
	 * @return the isOnSale
	 *
	 */
	public Integer getIsOnSale() {
		return isOnSale;
	}

	/**
	 * 设置 isOnSale的值
	 * 
	 * @param isOnSale
	 *            the isOnSale to set
	 */
	public void setIsOnSale(Integer isOnSale) {
		this.isOnSale = isOnSale;
	}

	/**
	 * 获取minPrice的值
	 *
	 * @return the minPrice
	 *
	 */
	public Double getMinPrice() {
		return minPrice;
	}

	/**
	 * 设置 minPrice的值
	 * 
	 * @param minPrice
	 *            the minPrice to set
	 */
	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	/**
	 * 获取maxPrice的值
	 *
	 * @return the maxPrice
	 *
	 */
	public Double getMaxPrice() {
		return maxPrice;
	}

	/**
	 * 设置 maxPrice的值
	 * 
	 * @param maxPrice
	 *            the maxPrice to set
	 */
	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}

	/**
	 * 获取minNum的值
	 *
	 * @return the minNum
	 *
	 */
	public Integer getMinNum() {
		return minNum;
	}

	/**
	 * 设置 minNum的值
	 * 
	 * @param minNum
	 *            the minNum to set
	 */
	public void setMinNum(Integer minNum) {
		this.minNum = minNum;
	}

	/**
	 * 获取maxNum的值
	 *
	 * @return the maxNum
	 *
	 */
	public Integer getMaxNum() {
		return maxNum;
	}

	/**
	 * 设置 maxNum的值
	 * 
	 * @param maxNum
	 *            the maxNum to set
	 */
	public void setMaxNum(Integer maxNum) {
		this.maxNum = maxNum;
	}

}
