/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.form;

/**
 * 商品销售统计信息查询表单
 * @author donghongli
 * @version $Id: StaGoodsOrderForm.java, v 0.1 2016年1月12日 下午12:10:36 donghongli Exp $
 */
public class StaGoodsOrderForm {

    private String day;

    private String month;

    private String year;

    private Long   goodsId;

    private String goodsName;

}
