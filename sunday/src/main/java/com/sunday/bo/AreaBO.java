/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunday.dao.AreaDOMapper;
import com.sunday.pojo.AreaDO;
import com.sunday.result.BizException;
import com.sunday.result.enums.BizResultCodeEnum;

/**
 * 地区BO
 * @author donghongli
 * @version $Id: AreaBO.java, v 0.1 2016年1月27日 上午11:01:00 donghongli Exp $
 */
@Service
public class AreaBO {

    @Autowired
    private AreaDOMapper areaDOMapper;

    public List<AreaDO> queryByParentId(Integer parentId) {
        if (parentId == null) {
            parentId = 0;
        }
        return areaDOMapper.queryByParentId(parentId);
    }

    public AreaDO find(Integer areaId) {
        return areaDOMapper.selectByPrimaryKey(areaId);
    }

    public void saveOrUpdate(AreaDO areaDO) {
        if (areaDO.getId() == null) {
            areaDOMapper.insertSelective(areaDO);
        } else {
            AreaDO oldArea = areaDOMapper.selectByPrimaryKey(areaDO.getId());
            if (oldArea == null) {
                throw new BizException(BizResultCodeEnum.NOT_EXISTED, "地区不存在areaId="
                                                                      + areaDO.getId());
            }
            areaDOMapper.updateByPrimaryKeySelective(areaDO);
        }
    }

    public void delete(int areaId) {
        areaDOMapper.deleteByPrimaryKey(areaId);
    }

}
