/**
 * 
 */
package com.sunday.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunday.dao.GoodsDOMapper;
import com.sunday.form.GoodsQueryForm;
import com.sunday.pojo.GoodsDO;
import com.sunday.result.BaseResult;
import com.sunday.result.BizException;
import com.sunday.result.enums.BizResultCodeEnum;

/**
 * @author donghongli
 *
 */
@Service
public class GoodsBO {

	@Autowired
	private GoodsDOMapper goodsDOMapper;

	/**
	 * 列表查询
	 * 
	 * @param form
	 * @return BaseResult<List<GoodsDO>>
	 * @exception
	 */
	public BaseResult<List<GoodsDO>> queryList(GoodsQueryForm form) {
		List<GoodsDO> goodsList = goodsDOMapper.selectActive(form);
		int totalRows = goodsDOMapper.selectActiveCount(form);
		return new BaseResult<List<GoodsDO>>(goodsList, form, totalRows);
	}

	/**
	 * 保存或修改
	 * 
	 * @param goodsDO
	 *            void
	 * @exception
	 */
	public void saveOrUpdate(GoodsDO goodsDO) {
		if (goodsDO.getId() == null) {
			goodsDOMapper.insert(goodsDO);
		} else {
			GoodsDO oldDO = goodsDOMapper.selectByPrimaryKey(goodsDO.getId());
			if (oldDO == null) {
				throw new BizException(BizResultCodeEnum.NOT_EXISTED,
						"要修改的数据不存在，goodsId=" + goodsDO.getId());
			}
			goodsDOMapper.updateByPrimaryKeySelective(goodsDO);
		}
	}

	/**
	 * 单个商品详情查询
	 * 
	 * @param goodsId
	 * @return GoodsDO
	 * @exception
	 */
	public GoodsDO getById(Long goodsId) {
		return goodsDOMapper.selectByPrimaryKey(goodsId);
	}
}
