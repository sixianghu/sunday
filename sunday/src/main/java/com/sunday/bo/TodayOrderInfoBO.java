/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.bo;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunday.dao.TodayOrderInfoDOMapper;
import com.sunday.utils.DateUtil;

/**
 * 今天订单(导入今天订单数据,但表中存储的是最近30天的订单数据)详情(临时表)
 * @author donghongli
 * @version $Id: TodayOrderInfoBO.java, v 0.1 2016年2月25日 下午2:55:43 donghongli Exp $
 */
@Service
public class TodayOrderInfoBO {

    @Autowired
    private TodayOrderInfoDOMapper todayOrderInfoDOMapper;

    public void importTodayOrderInfo() {

        cleanMoreThen30DaysOrderInfo();

        if (!checkIsHaveTodayOrderInfo()) {
            todayOrderInfoDOMapper.importTodayOrderInfo();
        }
    }

    /**
     * 清除超过30天的订单信息
     */
    private void cleanMoreThen30DaysOrderInfo() {
        Date last30DayTime = DateUtils.addDays(new Date(), -30);
        String last30Date = DateUtil.format(last30DayTime, DateUtil.DATE_SIMPLE_FORMAT);
        todayOrderInfoDOMapper.cleanMoreThen30DaysOrderInfo(last30Date);
    }

    /**
     * 判断今天的订单信息是否已经导入true:已导入 false:未导入
     * 
     * @return
     */
    private boolean checkIsHaveTodayOrderInfo() {
        Date lasttime = todayOrderInfoDOMapper.lastImportDate();
        if (DateUtils.isSameDay(lasttime, new Date())) {
            return true;
        }
        return false;
    }
}
