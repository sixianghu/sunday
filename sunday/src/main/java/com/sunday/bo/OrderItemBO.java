/**
 * 万色城基础服务系统
 * 2015年11月13日-下午3:14:11
 * 2015杭州万色城电子商务有限公司-版权所有
 * 
 */
package com.sunday.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunday.dao.OrderItemDOMapper;
import com.sunday.form.OrderItemQueryForm;
import com.sunday.pojo.OrderItemDO;
import com.sunday.pojo.StaUserOrderDO;

/**
 * 订单项
 * 
 * @author donghongli 2015年11月13日 下午3:14:11
 * @version 1.1.0
 * 
 */
@Service
public class OrderItemBO {

    @Autowired
    private OrderItemDOMapper orderItemDOMapper;

    public List<OrderItemDO> list(OrderItemQueryForm form) {
        return orderItemDOMapper.list(form);
    }

    /**
     * 统计指定月份用户进货情况
     * @param month
     * @return
     */
    public List<StaUserOrderDO> staUserByMonth(String month) {
        return orderItemDOMapper.staUserByMonth(month);
    }

}
