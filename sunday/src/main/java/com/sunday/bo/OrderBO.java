/**
 * 万色城基础服务系统
 * 2015年11月13日-下午3:14:00
 * 2015杭州万色城电子商务有限公司-版权所有
 * 
 */
package com.sunday.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunday.dao.OrderDOMapper;
import com.sunday.form.OrderItemQueryForm;
import com.sunday.form.OrderQueryForm;
import com.sunday.pojo.OrderDO;
import com.sunday.pojo.OrderItemDO;
import com.sunday.result.BaseResult;

/**
 * 订单BO
 * 
 * @author donghongli 2015年11月13日 下午3:14:00
 * @version 1.1.0
 * 
 */
@Service
public class OrderBO {

    @Autowired
    private OrderDOMapper orderDOMapper;
    @Autowired
    private OrderItemBO   orderItemBO;

    public BaseResult<List<OrderDO>> pageQuery(OrderQueryForm form) {
        List<OrderDO> orderList = orderDOMapper.list(form);
        int totalRows = orderDOMapper.count(form);
        return new BaseResult<List<OrderDO>>(orderList, form, totalRows);
    }

    public List<OrderDO> queryList(OrderQueryForm form) {
        return orderDOMapper.list(form);
    }

    public OrderDO find(Long orderId) {
        OrderDO orderDO = orderDOMapper.selectByPrimaryKey(orderId);
        if (orderDO != null) {
            OrderItemQueryForm itemForm = new OrderItemQueryForm();
            itemForm.setOrderId(orderId);
            List<OrderItemDO> items = orderItemBO.list(itemForm);
            orderDO.setItems(items);
        }
        return orderDO;
    }
}
