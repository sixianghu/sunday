/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunday.dao.StaGoodsOrderDOMapper;
import com.sunday.pojo.StaGoodsOrderDO;

/**
 * 商品订单销售统计
 * @author donghongli
 * @version $Id: StaGoodsOrderBO.java, v 0.1 2016年1月11日 上午11:08:26 donghongli Exp $
 */
@Service
public class StaGoodsOrderBO {

    @Autowired
    private StaGoodsOrderDOMapper staGoodsOrderDOMapper;

    /**
     * 保存单条订单销售统计信息
     * @param staGoodsOrderDO
     */
    public void save(StaGoodsOrderDO staGoodsOrderDO) {
        staGoodsOrderDOMapper.insert(staGoodsOrderDO);
    }

    /**
     * 
     * @param staGoodsOrderDO
     */
    public void update(StaGoodsOrderDO staGoodsOrderDO) {
        staGoodsOrderDOMapper.updateByPrimaryKeySelective(staGoodsOrderDO);
    }

    /**
     * 
     * @param goodsId
     * @return
     */
    public List<StaGoodsOrderDO> list(long goodsId) {
        return staGoodsOrderDOMapper.list(goodsId);
    }

    /**
     * 统计今天商品销售信息
     */
    public void importTodayGoodsSaleInfo() {
        staGoodsOrderDOMapper.importTodayGoodsSaleInfo();
    }
}
