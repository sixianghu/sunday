/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunday.dao.StaUserOrderDOMapper;
import com.sunday.pojo.StaUserOrderDO;

/**
 * 用户进货信息统计
 * @author donghongli
 * @version $Id: StaUserOrderBO.java, v 0.1 2016年1月12日 上午11:22:11 donghongli Exp $
 */
@Service
public class StaUserOrderBO {

    @Autowired
    private StaUserOrderDOMapper staUserOrderDOMapper;

    /**
     * 保存用户进货统计信息
     * @param staUserOrderDO
     */
    public void save(StaUserOrderDO staUserOrderDO) {
        staUserOrderDOMapper.insertSelective(staUserOrderDO);
    }

    /**
     * 查询用户最近month月内的销售情况
     * @param monthNum
     * @param userId 
     * @return
     */
    public List<StaUserOrderDO> lastTimeMonths(int monthNum, long userId) {
        return staUserOrderDOMapper.lastTimeDays(monthNum, userId);
    }

}
