/**
 * 
 */
package com.sunday.base;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 分页返回对象
 * 
 * @author donghongli
 *
 */
public class Page {

    protected final Logger   logger            = LoggerFactory.getLogger(Page.class);

    /**
     * 默认分页大小
     */
    private static final int DEFAULT_PAGE_SIZE = 20;

    /**
     * 默认起始页码
     */
    private static final int DEFAULT_PAGE_NO   = 1;

    /**
     * 当前页码
     */
    private int              currentPage       = DEFAULT_PAGE_NO;

    /**
     * 每页记录数
     */
    private int              rows              = DEFAULT_PAGE_SIZE;

    /**
     * 符合条件的总条数
     */
    private int              totalRows;

    /**
     * 搜索列表url
     */
    private String           url;

    /**
     * 排序字段
     */
    private String           orderBy           = "modify";

    /**
     * 排序类型
     */
    private String           orderType         = "desc";

    /**
     * 空构造
     */
    public Page() {
    }

    /**
     * 含分页参数的构造
     * 
     * @param currentPage
     * @param size
     */
    public Page(int currentPage, int rows) {
        this.currentPage = currentPage;
        this.rows = rows;
    }

    /**
     * @return the currentPage
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * @param currentPage
     *            the currentPage to set
     */
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage >= 1 ? currentPage : DEFAULT_PAGE_NO;
    }

    /**
     * @return the orderBy
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * 获取rows的值
     *
     * @return the rows
     *
     */
    public int getRows() {
        return rows;
    }

    /**
     * 设置 rows的值
     * 
     * @param rows
     *            the rows to set
     */
    public void setRows(int rows) {
        this.rows = rows > 1 ? rows : DEFAULT_PAGE_SIZE;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the totalRows
     */
    public int getTotalRows() {
        return totalRows;
    }

    /**
     * @param totalRows
     *            the totalRows to set
     */
    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    /**
     * @param orderBy
     *            the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * @return the begin
     */
    public int getBegin() {
        return (currentPage - 1) * rows;
    }

    public int getTotalPages() {
        int pages = totalRows / rows;
        return pages < 1 ? 1 : (totalRows % rows > 0 ? pages + 1 : pages);
    }

    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType
     *            the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
