package com.sunday.pojo;

import java.util.Date;
import java.util.List;

public class OrderDO {
    private Long id;

    private String ordersn;

    private Long buyerId;

    private String buyerName;

    private Long sellerId;

    private String sellerName;

    private Long cashierId;

    private String cashierName;

	private Integer type;

	private Integer status;

	private Integer payStatus;

    private Date payTime;

    private Double amount;

    private Double arrearsAmount;

    private Double realAmount;

    private String returnSn;

	private Integer isValid;

    private Date sendTime;

    private Date created;

    private Date modifyd;

	private List<OrderItemDO> items;

	/**
	 * 获取items的值
	 *
	 * @return the items
	 *
	 */
	public List<OrderItemDO> getItems() {
		return items;
	}

	/**
	 * 设置 items的值
	 * 
	 * @param items
	 *            the items to set
	 */
	public void setItems(List<OrderItemDO> items) {
		this.items = items;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrdersn() {
        return ordersn;
    }

    public void setOrdersn(String ordersn) {
        this.ordersn = ordersn;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getCashierId() {
        return cashierId;
    }

    public void setCashierId(Long cashierId) {
        this.cashierId = cashierId;
    }

    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }


	/**
	 * 获取type的值
	 *
	 * @return the type
	 *
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * 设置 type的值
	 * 
	 * @param type
	 *            the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * 获取status的值
	 *
	 * @return the status
	 *
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * 设置 status的值
	 * 
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 获取payStatus的值
	 *
	 * @return the payStatus
	 *
	 */
	public Integer getPayStatus() {
		return payStatus;
	}

	/**
	 * 设置 payStatus的值
	 * 
	 * @param payStatus
	 *            the payStatus to set
	 */
	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getArrearsAmount() {
        return arrearsAmount;
    }

    public void setArrearsAmount(Double arrearsAmount) {
        this.arrearsAmount = arrearsAmount;
    }

    public Double getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Double realAmount) {
        this.realAmount = realAmount;
    }

    public String getReturnSn() {
        return returnSn;
    }

    public void setReturnSn(String returnSn) {
        this.returnSn = returnSn;
    }


	/**
	 * 获取isValid的值
	 *
	 * @return the isValid
	 *
	 */
	public Integer getIsValid() {
		return isValid;
	}

	/**
	 * 设置 isValid的值
	 * 
	 * @param isValid
	 *            the isValid to set
	 */
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}

	public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModifyd() {
        return modifyd;
    }

    public void setModifyd(Date modifyd) {
        this.modifyd = modifyd;
    }
}