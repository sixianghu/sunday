package com.sunday.pojo;

import java.util.Date;

public class GoodsCateGoryDO {
    private Integer id;

    private String name;

    private Date created;

    private Date modifyd;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModifyd() {
        return modifyd;
    }

    public void setModifyd(Date modifyd) {
        this.modifyd = modifyd;
    }
}