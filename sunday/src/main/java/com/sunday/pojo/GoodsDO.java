package com.sunday.pojo;

import java.util.Date;

public class GoodsDO {
    private Long    id;

    private String  name;

    private String  color;

    private Integer type;

    private Integer size;

    private String  desc;

    private Integer number;

    private Integer minNumber;

    private Short   isStockRemind;

    private Short   isOnSale;

    private Double  price;

    private Double  salePrice;

    private Double  batchPrice;

    private Date    created;

    private Long    createdUserId;

    private Date    modifyd;

    private Long    modifydUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getMinNumber() {
        return minNumber;
    }

    public void setMinNumber(Integer minNumber) {
        this.minNumber = minNumber;
    }

    public Short getIsStockRemind() {
        return isStockRemind;
    }

    public void setIsStockRemind(Short isStockRemind) {
        this.isStockRemind = isStockRemind;
    }

    public Short getIsOnSale() {
        return isOnSale;
    }

    public void setIsOnSale(Short isOnSale) {
        this.isOnSale = isOnSale;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Double getBatchPrice() {
        return batchPrice;
    }

    public void setBatchPrice(Double batchPrice) {
        this.batchPrice = batchPrice;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getModifyd() {
        return modifyd;
    }

    public void setModifyd(Date modifyd) {
        this.modifyd = modifyd;
    }

    public Long getModifydUserId() {
        return modifydUserId;
    }

    public void setModifydUserId(Long modifydUserId) {
        this.modifydUserId = modifydUserId;
    }

}