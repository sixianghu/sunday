package com.sunday.dao;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.sunday.pojo.TodayOrderInfoDO;

public interface TodayOrderInfoDOMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TodayOrderInfoDO record);

    int insertSelective(TodayOrderInfoDO record);

    TodayOrderInfoDO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TodayOrderInfoDO record);

    int updateByPrimaryKey(TodayOrderInfoDO record);

    void importTodayOrderInfo();

    void cleanMoreThen30DaysOrderInfo(@Param("last30Date") String last30Date);

    /**
     * 获取表中最后导入数据的时间
     * 
     * @return
     */
    Date lastImportDate();
}