package com.sunday.dao;

import java.util.List;

import com.sunday.pojo.AreaDO;

public interface AreaDOMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AreaDO record);

    int insertSelective(AreaDO record);

    AreaDO selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AreaDO record);

    int updateByPrimaryKey(AreaDO record);

    List<AreaDO> queryByParentId(Integer parentId);
}