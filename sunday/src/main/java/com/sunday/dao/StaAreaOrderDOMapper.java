package com.sunday.dao;

import com.sunday.pojo.StaAreaOrderDO;

public interface StaAreaOrderDOMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StaAreaOrderDO record);

    int insertSelective(StaAreaOrderDO record);

    StaAreaOrderDO selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StaAreaOrderDO record);

    int updateByPrimaryKey(StaAreaOrderDO record);
}