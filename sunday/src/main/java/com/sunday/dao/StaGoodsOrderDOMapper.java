package com.sunday.dao;

import java.util.List;

import com.sunday.pojo.StaGoodsOrderDO;

public interface StaGoodsOrderDOMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StaGoodsOrderDO record);

    int insertSelective(StaGoodsOrderDO record);

    StaGoodsOrderDO selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StaGoodsOrderDO record);

    int updateByPrimaryKey(StaGoodsOrderDO record);

    List<StaGoodsOrderDO> list(long goodsId);

    void importTodayGoodsSaleInfo();
}