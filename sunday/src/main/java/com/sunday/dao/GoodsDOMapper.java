package com.sunday.dao;

import java.util.List;

import com.sunday.form.GoodsQueryForm;
import com.sunday.pojo.GoodsDO;

public interface GoodsDOMapper {

    int deleteByPrimaryKey(Long id);

    int insert(GoodsDO record);

    int insertSelective(GoodsDO record);

    GoodsDO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GoodsDO record);

    int updateByPrimaryKey(GoodsDO record);

	/**
	 * 
	 * @param form
	 * @return List<GoodsDO>
	 * @exception
	 */
	List<GoodsDO> selectActive(GoodsQueryForm form);

	/**
	 * 
	 * @param form
	 * @return int
	 * @exception
	 */
	int selectActiveCount(GoodsQueryForm form);
}