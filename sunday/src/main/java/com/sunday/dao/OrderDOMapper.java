package com.sunday.dao;

import java.util.List;

import com.sunday.form.OrderQueryForm;
import com.sunday.pojo.OrderDO;

public interface OrderDOMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrderDO record);

    int insertSelective(OrderDO record);

    OrderDO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderDO record);

    int updateByPrimaryKey(OrderDO record);

	/**
	 * 
	 * @param form
	 * @return List<OrderDO>
	 * @exception
	 */
	List<OrderDO> list(OrderQueryForm form);

	/**
	 * 
	 * @param form
	 * @return int
	 * @exception
	 */
	int count(OrderQueryForm form);
}