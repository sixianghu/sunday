package com.sunday.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sunday.pojo.StaUserOrderDO;

public interface StaUserOrderDOMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StaUserOrderDO record);

    int insertSelective(StaUserOrderDO record);

    StaUserOrderDO selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StaUserOrderDO record);

    int updateByPrimaryKey(StaUserOrderDO record);

    List<StaUserOrderDO> lastTimeDays(@Param("monthNum") int monthNum, @Param("userId") long userId);
}