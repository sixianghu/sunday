package com.sunday.dao;

import java.util.List;

import com.sunday.pojo.GoodsCateGoryDO;

public interface GoodsCateGoryDOMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GoodsCateGoryDO record);

    int insertSelective(GoodsCateGoryDO record);

    GoodsCateGoryDO selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GoodsCateGoryDO record);

    int updateByPrimaryKey(GoodsCateGoryDO record);

    List<GoodsCateGoryDO> list();
}