package com.sunday.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sunday.form.OrderItemQueryForm;
import com.sunday.pojo.OrderItemDO;
import com.sunday.pojo.StaUserOrderDO;

public interface OrderItemDOMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrderItemDO record);

    int insertSelective(OrderItemDO record);

    OrderItemDO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderItemDO record);

    int updateByPrimaryKey(OrderItemDO record);

    /**
     * 
     * @param form
     * @return List<OrderItemDO>
     * @exception
     */
    List<OrderItemDO> list(OrderItemQueryForm form);

    /**
     * 
     * @param month
     * @return
     */
    List<StaUserOrderDO> staUserByMonth(@Param("month") String month);

}