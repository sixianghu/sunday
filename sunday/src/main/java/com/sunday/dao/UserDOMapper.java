package com.sunday.dao;

import java.util.List;

import com.sunday.form.UserQueryForm;
import com.sunday.pojo.UserDO;

public interface UserDOMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserDO record);

    int insertSelective(UserDO record);

    UserDO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserDO record);

    int updateByPrimaryKey(UserDO record);

    List<UserDO> list(UserQueryForm form);

    int count(UserQueryForm form);
}