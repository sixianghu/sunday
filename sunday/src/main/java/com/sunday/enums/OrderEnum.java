/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.enums;

/**
 * 订单相关枚举
 * @author donghongli
 * @version $Id: OrderEnum.java, v 0.1 2016年2月29日 上午11:47:49 donghongli Exp $
 */
public class OrderEnum {

    /**
     * 订单类型
     * 
     * @author donghongli
     * @version $Id: OrderEnum.java, v 0.1 2016年2月29日 上午11:49:59 donghongli Exp $
     */
    public enum OrderTypeEnum {
        SALE_ORDER(1, "销售订单"),

        BUY_ORDER(2, "进货订单"),

        RETURN_ORDER(3, "退货订单");

        private int    code;
        private String name;

        private OrderTypeEnum(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public static OrderTypeEnum getByCode(int code) {
            for (OrderTypeEnum Enum : values()) {
                if (Enum.getCode() == code) {
                    return Enum;
                }
            }
            return null;
        }

    }

    /**
     * 订单状态
     * 
     * @author donghongli
     * @version $Id: OrderEnum.java, v 0.1 2016年2月29日 上午11:50:10 donghongli Exp $
     */
    public enum OrderStatus {

    }

    /**
     * 订单支付状态
     * 
     * @author donghongli
     * @version $Id: OrderEnum.java, v 0.1 2016年2月29日 上午11:50:23 donghongli Exp $
     */
    public enum OrderPayStatus {

    }

    /**
     * 订单是否有效
     * 
     * @author donghongli
     * @version $Id: OrderEnum.java, v 0.1 2016年2月29日 上午11:50:39 donghongli Exp $
     */
    public enum OrderIsValid {

    }
}
