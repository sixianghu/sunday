/**
 * www.ewanse.com.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package com.sunday.enums;

import org.apache.commons.lang.StringUtils;

/**
 * 用户类型枚举
 * @author donghongli
 * @version $Id: UserTypeEnum.java, v 0.1 2016年1月26日 下午6:24:20 donghongli Exp $
 */
public enum UserTypeEnum {

    SUPPLIER(1, "供货商"),

    SELLER(2, "销售商"),

    EMPLOYEES(3, "员工"), ;

    private int    code;

    private String desc;

    private UserTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getByCode(int code) {
        for (UserTypeEnum userType : values()) {
            if (userType.getCode() == code) {
                return userType.getDesc();
            }
        }
        return StringUtils.EMPTY;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
